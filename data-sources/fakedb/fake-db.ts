
import { ShopitemDataSource } from "../interfaces/shopitem-data-source";
import { ShopItem } from "../../models/shopitem";
import { shopItems } from "./data";

export default class FakeDb implements ShopitemDataSource {
    addShopItem(shopItem: ShopItem): Promise<ShopItem> {
        shopItem.id = shopItems.length + 1;
        shopItems.push(shopItem);
        return Promise.resolve(shopItems[shopItems.length - 1]);
    }

    deleteAllFavorite(): Promise<boolean> {
        shopItems.map(si => si.favorite = false);
        return Promise.resolve(true);
    }

    getAllFavorites(): Promise<ShopItem[]> {
        return Promise.resolve(shopItems.filter(si => si.favorite));
    }

    getAllShopItems(): Promise<ShopItem[]> {
        return Promise.resolve(shopItems);
    }

    getShopItem(id: number): Promise<ShopItem> {
        const shopItem = shopItems.find((si) => si.id === id);
        if (!shopItem) {
            return Promise.reject("Shop Item not found with that id");
        }
        return Promise.resolve(shopItem);
    }

    setFavorite(favoriteObject: { id: number; favorite: boolean }): Promise<boolean> {
        const { id, favorite } = favoriteObject;

        const index = shopItems.findIndex(si => si.id === id);
        if (index > -1) {
            shopItems[index].favorite = favorite;
            return Promise.resolve(true);
        }
        throw "Id not found";

    }

}


