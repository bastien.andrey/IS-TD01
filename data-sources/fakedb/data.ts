import {ShopItem} from "../../models/shopitem";

const shopItems: ShopItem[] = [

    {id:1, price: 185.0, imageUrl: "https://i.ibb.co/M6hHc3F/brown-trench.png", name:"Tan Trench", category:"jacket", favorite: false},
    {price: 160.0, imageUrl: "https://i.ibb.co/QcvzydB/nikes-red.png", name:" Nike RedHigh Tops", id:2, category:"sneaker", favorite: false},
    {price: 280, imageUrl: "https://i.ibb.co/dJbG1cT/yeezy.png", name:"Adidas Yeezy", id:3, category:"sneaker", favorite: false},
    {price: 90.0, imageUrl: "https://i.ibb.co/mJS6vz0/blue-jean-jacket.png", name:" BlueJean Jacket", id:4, category:"jacket", favorite: false},
    {price: 125.0, imageUrl: "https://i.ibb.co/XzcwL5s/black-shearling.png", name:" BlackJean Shearling", id:5, category:"jacket", favorite: false},
    {price: 160, imageUrl:"https://i.ibb.co/1RcFPk0/white-nike-high-tops.png", name:"NikeWhite AirForce", id:6, category:"sneaker", favorite: false},
    {price: 200, imageUrl: "https://i.ibb.co/Mhh6wBg/timberlands.png", name:"Timberlands", id:7, category:"sneaker", favorite: false},
    {price: 18, imageUrl: "https://i.ibb.co/ypkgK0X/blue-beanie.png", name:"Blue Beanie", id:8, category:"hat", favorite: false},
    {price: 25, imageUrl: "https://i.ibb.co/ZYW3VTp/brown-brim.png", name:"Brown Brim", id:9, category:"hat", favorite: false},
    {price: 35, imageUrl: "https://i.ibb.co/QdJwgmp/brown-cowboy.png", name:"Brown Cowboy", id:10, category:"hat", favorite: false},
    {price: 190, imageUrl: "https://i.ibb.co/w4k6Ws9/nike-funky.png", name:" AirJordan Limited", id:11, category:"sneaker", favorite: false},
    {price: 160, imageUrl: "https://i.ibb.co/fMTV342/nike-brown.png", name:" Nike BrownHigh Tops", id:12, category:"sneaker", favorite: false},
    {price: 16, imageUrl: "https://i.ibb.co/X2VJP2W/blue-snapback.png", name:"Blue Snapback", id:13, category:"hat", favorite: false},
    {price: 14, imageUrl: "https://i.ibb.co/rKBDvJX/palm-tree-cap.png", name:" PalmTree Cap", id:14, category:"hat", favorite: false},
    {price: 14, imageUrl: "https://i.ibb.co/1f2nWMM/wolf-cap.png", name:"Wolf Cap", id:15, category:"hat", favorite: false},
    {price: 110, imageUrl: "https://i.ibb.co/bPmVXyP/black-converse.png", name:"Black Converse", id:16, category:"sneaker", favorite: false},
    {price: 18, imageUrl: "https://i.ibb.co/bLB646Z/red-beanie.png", name:"Red Beanie", id:17, category:"hat", favorite: false},
    {price: 165, imageUrl: "https://i.ibb.co/s96FpdP/brown-shearling.png", name:"Brown Shearling", id:18, category:"jacket", favorite: false},
    {price: 220, imageUrl: "https://i.ibb.co/0s3pdnc/adidas-nmd.png", name:"Adidas NMD", id:19, category:"sneaker", favorite: false},
    {price: 18, imageUrl: "https://i.ibb.co/YTjW3vF/green-beanie.png", name:"Green Beanie", id:20, category:"hat", favorite: false},
    {price: 90.0, imageUrl: "https://i.ibb.co/N71k1ML/grey-jean-jacket.png", name:" GreyJean Jacket", id: 21, category:"jacket", favorite: false},
    {price: 25, imageUrl: "https://i.ibb.co/RjBLWxB/grey-brim.png", name:"Grey Brim", id:22, category:"hat", favorite: false}


];

export {shopItems};
