import {ShopItem} from "../../models/shopitem";

export interface ShopitemDataSource {
    addShopItem(shopItem: ShopItem): Promise<ShopItem>;
    getShopItem(id:number): Promise<ShopItem>;
    getAllShopItems(): Promise<ShopItem[]>;
    setFavorite(favoriteObject: {id: number; favorite:boolean}): Promise<boolean>;
    getAllFavorites(): Promise<ShopItem[]>;
    deleteAllFavorite(): Promise<boolean>;
}