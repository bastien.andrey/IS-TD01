import fs from "fs";

import { Sequelize } from "sequelize";
import { ShopItem } from "../../models/shopitem";
import { SequelizeShopItem } from "./sequelizeShopItem";
import { ShopitemDataSource } from "../interfaces/shopitem-data-source";

export class MariadbHelper implements ShopitemDataSource {
    sequelize: Sequelize;
    constructor(sequelize: Sequelize) {
        this.sequelize = sequelize;
    }

    async connect() {
        try {
            console.log("Trying sequelize connection...");
            await this.sequelize.authenticate();
            console.log('Connection has been established successfully.');
            await this.init();
        } catch (error) {
            console.error('Unable to connect to the database:', error);
            throw error
        }
    }

    async init() {
        await this.populate();
    }

    async flush() {
        try {
            await this.sequelize.query("Delete from shopitems");
            await this.sequelize.query('ALTER TABLE shopitems AUTO_INCREMENT=1');
        } catch (err: any) {
            throw new Error(
                `Error flushing db: ${err.message}`
            );
        }
    }

    async populate() {
        try {
            const dataSql = fs.readFileSync('./data-sources/sequelize/init.sql', 'utf8').toString();
            await this.sequelize.query(dataSql);

        } catch (err: any) {
            throw new Error(
                `Error populate database: ${err.message}`
            );
        }
    }

    async close() {
        await this.sequelize.close();
    }

    async addShopItem(shopItem: ShopItem): Promise<ShopItem> {
        return await SequelizeShopItem.create({
            name: shopItem.name,
            price: shopItem.price,
            category: shopItem.category,
            imageUrl: shopItem.imageUrl,
            favorite: shopItem.favorite
        }
        );
    }
    async getShopItem(id: number): Promise<ShopItem> {
        const shopItem = await SequelizeShopItem.findByPk(id);
        if (!shopItem) {
            throw "No shopItem found";
        }
        return shopItem;
    }
    async getAllShopItems(): Promise<ShopItem[]> {
        try {
            return await SequelizeShopItem.findAll();
        }
        catch (e) {
            console.log(e);
            throw e;
        }
    }
    async getAllFavorites(): Promise<ShopItem[]> {
        return await SequelizeShopItem.findAll({
            where: {
                favorite: true
            }
        })
    }
    async setFavorite(favoriteObject: { id: number; favorite: boolean; }): Promise<boolean> {
        await SequelizeShopItem.update({ favorite: favoriteObject.favorite },
            {
                where: {
                    id: favoriteObject.id
                }
            });
        return true;
    }
    async deleteAllFavorite(): Promise<boolean> {
        await SequelizeShopItem.update({ favorite: false },
            {
                where: {
                    favorite: true
                }
            });
        return true;
    }
}