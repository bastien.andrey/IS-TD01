import { Options, } from 'sequelize';

export const devDB: Options = {
    dialect: 'mariadb',
    host: 'localhost',
    port: 7009,
    username: "root",
    password: "rootsi",
    database: "ecomstore",
    dialectOptions: {
        multipleStatements: true
    }
}