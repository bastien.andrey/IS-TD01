DROP TABLE `shopitems`;

CREATE TABLE IF NOT EXISTS `shopitems` (
                                           `id` INT NOT NULL AUTO_INCREMENT,
                                           `name` VARCHAR(255),
    `category` VARCHAR(20),
    `imageUrl` VARCHAR(255),
    `price` FLOAT,
    `favorite` BOOLEAN,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB;


insert into ecomstore.shopitems values (1, 'Tan Trench', 'jacket', 'https://i.ibb.co/M6hHc3F/brown-trench.png', 185.0, false);
insert into ecomstore.shopitems values (2, 'Nike RedHigh Tops', 'sneaker', 'https://i.ibb.co/QcvzydB/nikes-red.png', 160, false);
insert into ecomstore.shopitems values (3, 'Adidas Yeezy', 'sneaker', 'https://i.ibb.co/dJbG1cT/yeezy.png', 280, false);
insert into ecomstore.shopitems values (4, 'BlueJean Jacket', 'jacket', 'https://i.ibb.co/mJS6vz0/blue-jean-jacket.png', 90, false);
insert into ecomstore.shopitems values (5, 'BlackJean Shearling', 'jacket', 'https://i.ibb.co/XzcwL5s/black-shearling.png', 125, false);
insert into ecomstore.shopitems values (6, 'NikeWhite AirForce', 'sneaker', 'https://i.ibb.co/1RcFPk0/white-nike-high-tops.png', 160, false);
insert into ecomstore.shopitems values (7, 'Timberlands', 'sneaker', 'https://i.ibb.co/Mhh6wBg/timberlands.png', 200, false);
insert into ecomstore.shopitems values (8, 'Blue Beanie', 'hat', 'https://i.ibb.co/ypkgK0X/blue-beanie.png', 18, false);
insert into ecomstore.shopitems values (9, 'Brown Brim', 'hat', 'https://i.ibb.co/ZYW3VTp/brown-brim.png', 25, false);
insert into ecomstore.shopitems values (10, 'Brown Cowboy', 'hat', 'https://i.ibb.co/QdJwgmp/brown-cowboy.png', 35, false);
insert into ecomstore.shopitems values (11, 'AirJordan Limited', 'sneaker', 'https://i.ibb.co/w4k6Ws9/nike-funky.png', 190, false);
insert into ecomstore.shopitems values (12, 'Nike BrownHigh Tops', 'sneaker', 'https://i.ibb.co/fMTV342/nike-brown.png', 160, false);
insert into ecomstore.shopitems values (13, 'Blue Snapback', 'hat', 'https://i.ibb.co/X2VJP2W/blue-snapback.png', 16, false);
insert into ecomstore.shopitems values (14, 'PalmTree Cap', 'hat', 'https://i.ibb.co/rKBDvJX/palm-tree-cap.png', 14, false);
insert into ecomstore.shopitems values (15, 'Wolf Cap', 'hat', 'https://i.ibb.co/1f2nWMM/wolf-cap.png', 14, false);
insert into ecomstore.shopitems values (16, 'Black Converse', 'sneaker', 'https://i.ibb.co/bPmVXyP/black-converse.png', 110, false);
insert into ecomstore.shopitems values (17, 'Red Beanie', 'hat', 'https://i.ibb.co/bLB646Z/red-beanie.png', 18, false);
insert into ecomstore.shopitems values (18, 'Brown Shearling', 'jacket', 'https://i.ibb.co/s96FpdP/brown-shearling.png', 165, false);
insert into ecomstore.shopitems values (19, 'Adidas NMD', 'sneaker', 'https://i.ibb.co/0s3pdnc/adidas-nmd.png', 220, false);
insert into ecomstore.shopitems values (20, 'Green Beanie', 'hat', 'https://i.ibb.co/YTjW3vF/green-beanie.png', 18, false);
insert into ecomstore.shopitems values (21, 'GreyJean Jacket', 'jacket', 'https://i.ibb.co/N71k1ML/grey-jean-jacket.png', 90, false);
insert into ecomstore.shopitems values (22, 'Grey Brim', 'hat', 'https://i.ibb.co/RjBLWxB/grey-brim.png', 25, false);
