import { CreationOptional, DataTypes, InferAttributes, InferCreationAttributes, Model } from "sequelize";
import { sequelize } from "./sequelize";


export class SequelizeShopItem extends Model<InferAttributes<SequelizeShopItem>, InferCreationAttributes<SequelizeShopItem>>{
    declare id: CreationOptional<number>;
    declare name: string;
    declare category: string;
    declare imageUrl: string;
    declare price: number;
    declare favorite: boolean;
}


SequelizeShopItem.init({
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    name: {
        type: DataTypes.TEXT,
    },
    category: {
        type: DataTypes.TEXT,
    },
    imageUrl: {
        type: DataTypes.TEXT
    },
    price: {
        type: DataTypes.FLOAT
    },
    favorite: {
        type: DataTypes.BOOLEAN
    },
}, {
    sequelize,
    tableName: "shopitems",
    timestamps: false
});
