import { ShopitemRepository } from "./repositories/shopitem-repository";
import FakeDb from "./data-sources/fakedb/fake-db";
import { ShopitemController } from "./controllers/shopitem-controller";
import { AddShopItemUseCase } from "./use-cases/shopItems/add-shop-item-use-case";
import { GetAllFavoritesUseCase } from "./use-cases/shopItems/get-all-favorites-use-case";
import { GetAllShopItemsUseCase } from "./use-cases/shopItems/get-all-shopItems-use-case";
import { GetShopItemUseCase } from "./use-cases/shopItems/get-shopItem-use-case";
import { DeleteAllFavoritesUseCase } from "./use-cases/shopItems/delete-all-favorites-use-case";
import { SetFavoriteUseCase } from "./use-cases/shopItems/set-favorite-use-case";
import { sendUnaryData, ServerUnaryCall } from "@grpc/grpc-js";
import * as grpc from "@grpc/grpc-js"
import * as protoLoader from "@grpc/proto-loader";
import { Sequelize } from "sequelize";
import { MariadbHelper } from "./data-sources/sequelize/mariadb-helper";
import { devDB } from "./data-sources/sequelize/config";

(async () => {

    const sequelize = new Sequelize(devDB);
    const mariadbDataSource = new MariadbHelper(sequelize);
    await mariadbDataSource.connect();
    const repository = new ShopitemRepository(mariadbDataSource);
    const controller = new ShopitemController(new AddShopItemUseCase(repository),
        new GetShopItemUseCase(repository),
        new GetAllShopItemsUseCase(repository),
        new GetAllFavoritesUseCase(repository),
        new SetFavoriteUseCase(repository),
        new DeleteAllFavoritesUseCase(repository)
    )

    const packageDefinition = protoLoader.loadSync('./grpc/proto/shopItem.proto', {});
    const shopItemsProto = grpc.loadPackageDefinition(packageDefinition);
    const server = new grpc.Server();

    //@ts-ignore
    server.addService(shopItemsProto.ShopItemsService.service, {
        addShopItem: (async (call: ServerUnaryCall<any, any>, callback: sendUnaryData<any>) => {
            console.log(call.request);
            callback(null, await controller.addShopItem(call.request.shopItem));
        }),
        getShopItem: (async (call: ServerUnaryCall<any, any>, callback: sendUnaryData<any>) => {
            callback(null, await controller.getShopItem(call.request.id));
        }),
        getAllShopItems: (async (call: ServerUnaryCall<any, any>, callback: sendUnaryData<any>) => {
            callback(null, { "shopItems": await controller.getAllShopItems() });
        }),
        getAllFavorites: (async (call: ServerUnaryCall<any, any>, callback: sendUnaryData<any>) => {
            callback(null, { "shopItems": await controller.getAllFavorites() });
        }),
        setFavorite: (async (call: ServerUnaryCall<any, any>, callback: sendUnaryData<any>) => {
            await controller.setFavorite(call.request)
            callback(null, { "message": `ShopItem id ${call.request.id} set successfully!` });
        }),
        deleteAllFavorite: (async (call: ServerUnaryCall<any, any>, callback: sendUnaryData<any>) => {
            await controller.deleteAllFavorite()
            callback(null, { "message": "Removed all favorites successfully!" });
        })
    });
    server.bindAsync('0.0.0.0:5019', grpc.ServerCredentials.createInsecure(), () => {
        console.log("Server running at http://127.0.0.1:5019");
        server.start();
    });
})()