import { ShopitemRepositoryInterface } from "./shopitem-repository-interface";
import { ShopItem } from "../models/shopitem";
import { ShopitemDataSource } from "../data-sources/interfaces/shopitem-data-source";

// Repository must get data and save data in different data sources
export class ShopitemRepository implements ShopitemRepositoryInterface {

    shopItemDataSource: ShopitemDataSource;


    constructor(shopItemDataSource: ShopitemDataSource) {
        this.shopItemDataSource = shopItemDataSource;
    }


    async addShopItem(shopItem: ShopItem): Promise<ShopItem> {
        return await this.shopItemDataSource.addShopItem(shopItem);
    }

    async deleteAllFavorite(): Promise<boolean> {
        return await this.shopItemDataSource.deleteAllFavorite();
    }

    async getAllFavorites(): Promise<ShopItem[]> {
        return await this.shopItemDataSource.getAllFavorites();
    }

    async getAllShopItems(): Promise<ShopItem[]> {
        return await this.shopItemDataSource.getAllShopItems();
    }

    async getShopItem(id: number): Promise<ShopItem> {
        return await this.shopItemDataSource.getShopItem(id);
    }

    async setFavorite(favoriteObject: { id: number; favorite: boolean }): Promise<boolean> {
        return await this.shopItemDataSource.setFavorite(favoriteObject);
    }

}