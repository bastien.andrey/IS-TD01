export interface ShopItem {
    id: number
    name: string,
    category: string,
    imageUrl: string,
    price: number,
    favorite: boolean,
}