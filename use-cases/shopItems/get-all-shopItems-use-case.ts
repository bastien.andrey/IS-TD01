import {ShopitemRepository} from "../../repositories/shopitem-repository";
import {ShopItem} from "../../models/shopitem";

export class GetAllShopItemsUseCase{
    shopItemRepository: ShopitemRepository;

    constructor(shopItemsRepository: ShopitemRepository) {
        this.shopItemRepository = shopItemsRepository;
    }

    async execute(): Promise<ShopItem[]>{
        return await this.shopItemRepository.getAllShopItems();
    }

}