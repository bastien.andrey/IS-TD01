import { ShopitemRepository } from "../../repositories/shopitem-repository";
import { ShopItem } from "../../models/shopitem";

export class AddShopItemUseCase {
    shopItemRepository: ShopitemRepository;

    constructor(shopItemsRepository: ShopitemRepository) {
        this.shopItemRepository = shopItemsRepository;
    }

    async execute(shopItem: ShopItem): Promise<ShopItem> {
        return await this.shopItemRepository.addShopItem(shopItem);
    }

}