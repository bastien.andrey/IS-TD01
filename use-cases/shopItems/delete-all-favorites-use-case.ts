import {ShopitemRepository} from "../../repositories/shopitem-repository";



export class DeleteAllFavoritesUseCase{
    shopItemRepository: ShopitemRepository;

    constructor(shopItemsRepository: ShopitemRepository) {
        this.shopItemRepository = shopItemsRepository;
    }

    async execute(): Promise<boolean>{
        return await this.shopItemRepository.deleteAllFavorite();
    }

}