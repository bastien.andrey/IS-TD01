import {ShopitemRepository} from "../../repositories/shopitem-repository";

export class SetFavoriteUseCase{
    shopItemRepository: ShopitemRepository;

    constructor(shopItemsRepository: ShopitemRepository) {
        this.shopItemRepository = shopItemsRepository;
    }

    async execute(favoriteObject: {id:number; favorite: boolean}): Promise<boolean>{
        return await this.shopItemRepository.setFavorite(favoriteObject);
    }

}