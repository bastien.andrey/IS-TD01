import {ShopitemRepository} from "../../repositories/shopitem-repository";
import {ShopItem} from "../../models/shopitem";

export class GetShopItemUseCase{
    shopItemRepository: ShopitemRepository;

    constructor(shopItemsRepository: ShopitemRepository) {
        this.shopItemRepository = shopItemsRepository;
    }

    async execute(id:number): Promise<ShopItem>{
        return await this.shopItemRepository.getShopItem(id);
    }

}