import { AddShopItemUseCase } from "../use-cases/shopItems/add-shop-item-use-case";
import { GetShopItemUseCase } from "../use-cases/shopItems/get-shopItem-use-case";
import { GetAllShopItemsUseCase } from "../use-cases/shopItems/get-all-shopItems-use-case";
import { GetAllFavoritesUseCase } from "../use-cases/shopItems/get-all-favorites-use-case";
import { SetFavoriteUseCase } from "../use-cases/shopItems/set-favorite-use-case";
import { DeleteAllFavoritesUseCase } from "../use-cases/shopItems/delete-all-favorites-use-case";
import { ShopItem } from "../models/shopitem";

export class ShopitemController {

    addShopItemUseCase: AddShopItemUseCase;
    getShopItemUseCase: GetShopItemUseCase;
    getAllShopItemUseCase: GetAllShopItemsUseCase;
    getAllFavoritesUseCase: GetAllFavoritesUseCase;
    setFavoriteUseCase: SetFavoriteUseCase;
    deleteAllFavoritesUseCase: DeleteAllFavoritesUseCase;


    constructor(addShopItemUseCase: AddShopItemUseCase,
        getShopItemUseCase: GetShopItemUseCase,
        getAllShopItemUseCase: GetAllShopItemsUseCase,
        getAllFavoritesUseCase: GetAllFavoritesUseCase,
        setFavoriteUseCase: SetFavoriteUseCase,
        deleteAllFavoritesUseCase: DeleteAllFavoritesUseCase) {
        this.addShopItemUseCase = addShopItemUseCase;
        this.getAllShopItemUseCase = getAllShopItemUseCase;
        this.getShopItemUseCase = getShopItemUseCase;
        this.getAllFavoritesUseCase = getAllFavoritesUseCase;
        this.setFavoriteUseCase = setFavoriteUseCase;
        this.deleteAllFavoritesUseCase = deleteAllFavoritesUseCase;
    }


    async addShopItem(shopItem: ShopItem) {
        console.log("shopitem received:", shopItem);
        console.log(shopItem);
        return await this.addShopItemUseCase.execute(shopItem);
    }

    async getShopItem(id: number) {
        return await this.getShopItemUseCase.execute(id);
    }

    async getAllShopItems() {
        return await this.getAllShopItemUseCase.execute();
    }

    async getAllFavorites() {
        return await this.getAllFavoritesUseCase.execute();
    }

    async setFavorite(favoriteObject: { id: number; favorite: boolean }) {
        return await this.setFavoriteUseCase.execute(favoriteObject);
    }

    async deleteAllFavorite() {
        return await this.deleteAllFavoritesUseCase.execute();
    }


}